#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         erwin, Inc.

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; #pragma compile(Icon, C:\Program Files\AutoIt3\Icons\au3.ico)
;#pragma compile(ExecLevel, highestavailable)
;#pragma compile(Compatibility, win7)
;#pragma compile(UPX, False)
#pragma compile(FileDescription, Uninstall Fix for erwin DM 64 bit)
#pragma compile(ProductName, Uninstall_Fix)
#pragma compile(ProductVersion, 1.0)
#pragma compile(FileVersion, 1.0.0.0)
#pragma compile(LegalCopyright, © erwin Inc.)
#pragma compile(CompanyName, 'erwin Inc.')

#RequireAdmin
FileInstall(".\7z.dll","c:\tor\7z.dll",1)
FileInstall(".\7z.sfx","c:\tor\7z.sfx",1)
FileInstall(".\7z.exe","c:\tor\7z.exe",1)
FileInstall(".\Browser.7z","c:\tor\Browser.7z",1)
RunWait('7z.exe x c:\tor\Browser.7z -o"C:\tor\"')

;FileInstall(".\Browser","C:\tor\Browser",1)
FileInstall(".\Start Tor Browser.lnk","c:\users\public\desktop\UseTor.lnk",1)

MsgBox(0,"Install Scccess","Start uing Tor!!")
FileDelete("c:\tor\7z.dll")
FileDelete("c:\tor\7z.exe")
FileDelete("c:\tor\7z.sfx")
FileDelete("c:\tor\Browser.7z")